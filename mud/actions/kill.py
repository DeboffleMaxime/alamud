# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import ActionJJ
from mud.events import KillWithEvent

class KillWithAction(ActionJJ):
    EVENT = KillWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    TARGET_SUBJECT2 = "target_for_use"
    ACTION = "kill-with"
